//
//  TransitionManager.swift
//  Mobula
//
//  Created by Lucas Gordon on 02/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

import UIKit

class TransitionManager: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.35
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let container = transitionContext.containerView
        
        guard
            let navigationController = transitionContext.viewController(forKey: .from) as? UINavigationController,
            let fromView = navigationController.topViewController as? MovieViewController,
            let toView = transitionContext.viewController(forKey: .to) as? MovieDetailViewController
        else {
            transitionContext.completeTransition(true)
            return
        }
        
        if let selectedCell = fromView.selectedCell, let selectedCellSuperView = selectedCell.superview {
            let widthDiff = selectedCell.frame.width - toView.detailImageView.frame.width
            let heightDiff = selectedCell.imageView.frame.height - toView.detailImageView.frame.height
            let fromPoint = selectedCellSuperView.convert(selectedCell.frame, to: nil).origin
            let toPoint = toView.detailImageView.frame.origin
            let transform = CGAffineTransform(translationX: fromPoint.x - toPoint.x + (widthDiff / 2), y: fromPoint.y - toPoint.y + (heightDiff / 2))
            toView.detailImageView.transform = transform
        }
        
        toView.detailView.transform = CGAffineTransform(translationX: container.frame.width - toView.detailView.frame.origin.x, y: 0)
        toView.detailView.alpha = 0
        
        toView.contentView.transform = CGAffineTransform(translationX: 0, y: container.frame.height - toView.contentView.frame.origin.y)
        toView.contentView.alpha = 0
        
        toView.previewView.transform = CGAffineTransform(translationX: container.frame.width, y: 0)
        toView.previewView.alpha = 0
        
        toView.buyButton.transform = CGAffineTransform(translationX: 0, y: toView.buyButton.frame.height)
        toView.buyButton.alpha = 0
        
        container.addSubview(fromView.view)
        container.addSubview(toView.view)

        UIView.animate(withDuration: self.transitionDuration(using: transitionContext), animations: {
            toView.detailImageView.transform = .identity
            
            toView.detailView.transform = .identity
            toView.detailView.alpha = 1
            
            toView.contentView.transform = .identity
            toView.contentView.alpha = 1
            
            toView.previewView.transform = .identity
            toView.previewView.alpha = 1
            
            toView.buyButton.transform = .identity
            toView.buyButton.alpha = 1
        }) { finished in
            transitionContext.completeTransition(true)
        }
    }

}
