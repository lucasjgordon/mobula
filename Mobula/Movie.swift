//
//  Movie.swift
//  Mobula
//
//  Created by Lucas Gordon on 01/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

import Foundation

struct Movie {
    let id: String
    let title: String
    let description: String
    let director: String
    let starring: String
    let thumb_url: URL
    let ticket_url: URL
    let trailer_cover_url: URL
    let trailer_youtube_id: String
}
