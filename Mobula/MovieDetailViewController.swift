//
//  MovieDetailViewController.swift
//  Mobula
//
//  Created by Lucas Gordon on 01/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {

    let buyButton = UIButton()
    let contentView = UIView()
    let detailImageView = UIImageView()
    let detailView = UIView()
    var movie: Movie!
    let previewView = UIView()
    var style: Style!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }

    func inject(style: Style) {
        self.style = style
    }

}

// MARK: Private methods
private extension MovieDetailViewController {
    
    @objc func buy() {
        UIApplication.shared.open(movie.ticket_url, options: [:], completionHandler: nil)
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func playPreview() {
        guard let url = URL(string: "https://www.youtube.com/watch?v=\(movie.trailer_youtube_id)") else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    func setupView() {
        view.backgroundColor = style.backgroundColor
        UIApplication.shared.statusBarStyle = style.statusBarStyle
        
        let scrollView = UIScrollView()
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        automaticallyAdjustsScrollViewInsets = false
        view.addSubview(scrollView)
        
        previewView.frame = CGRect(x: 0, y: -20, width: view.frame.width, height: 264)
        previewView.backgroundColor = style.preview.backgroundColor

        let previewImageView = UIImageView()
        previewImageView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 264)
        previewImageView.sd_setImage(with: movie.trailer_cover_url)
        previewImageView.contentMode = .scaleAspectFill
        previewImageView.clipsToBounds = true
        previewView.addSubview(previewImageView)
        
        let previewPlayButton = UIButton()
        previewPlayButton.frame = CGRect(x: (view.frame.width / 2) - (75 / 2), y: (previewView.frame.height / 2) - (75 / 2), width: 75, height: 75)
        previewPlayButton.setImage(style.preview.image, for: .normal)
        previewPlayButton.addTarget(self, action: #selector(self.playPreview), for: .touchUpInside)
        previewView.addSubview(previewPlayButton)
        
        scrollView.addSubview(previewView)
        
        let backButton = UIButton()
        backButton.frame = CGRect(x: 20, y: 33, width: 12, height: 20)
        backButton.setImage(UIImage(named: "back"), for: .normal)
        backButton.addTarget(self, action: #selector(self.close), for: .touchUpInside)
        scrollView.addSubview(backButton)

        detailImageView.frame = CGRect(x: 20, y: 206, width: 146, height: 215)
        detailImageView.sd_setImage(with: movie.thumb_url)
        detailImageView.backgroundColor = style.detail.imageBackgroundColor
        detailImageView.layer.cornerRadius = style.detail.imageCornerRadius
        detailImageView.contentMode = .scaleAspectFill
        detailImageView.clipsToBounds = true
        scrollView.addSubview(detailImageView)
        
        detailView.frame = CGRect(x: 190, y: 270, width: view.frame.width - 210, height: 149)
        
        let titleLabel = UILabel()
        titleLabel.frame = CGRect(x: 0, y: 0, width: detailView.frame.width, height: 21)
        titleLabel.text = movie.title
        titleLabel.font = style.detail.titleFont
        titleLabel.textColor = style.detail.titleColor
        detailView.addSubview(titleLabel)
        
        let runTimeLabel = UILabel()
        runTimeLabel.frame = CGRect(x: 0, y: titleLabel.frame.origin.y + titleLabel.frame.height + 5, width: detailView.frame.width, height: 18)
        runTimeLabel.text = "1hr 50m"
        runTimeLabel.font = style.detail.runTimeFont
        runTimeLabel.textColor = style.detail.runTimeColor
        detailView.addSubview(runTimeLabel)
        
        let genreLabel = UILabel()
        genreLabel.frame = CGRect(x: 0, y: runTimeLabel.frame.origin.y + runTimeLabel.frame.height + 9, width: detailView.frame.width, height: 14)
        genreLabel.text = "SCIENCE FICTION"
        genreLabel.font = style.detail.genreFont
        genreLabel.textColor = style.detail.genreColor
        detailView.addSubview(genreLabel)
        
        let priceLabel = UILabel()
        priceLabel.frame = CGRect(x: 0, y: genreLabel.frame.origin.y + genreLabel.frame.height + 10, width: detailView.frame.width, height: 21)
        priceLabel.text = "£14.00"
        priceLabel.font = style.detail.priceFont
        priceLabel.textColor = style.detail.priceColor
        detailView.addSubview(priceLabel)
        
        // if show imdb, add imdb
        // if show stars, add stars
        
        scrollView.addSubview(detailView)
        
        let descriptionTitleLabel = UILabel()
        descriptionTitleLabel.frame = CGRect(x: 0, y: 0, width: 200, height: 19)
        descriptionTitleLabel.text = "DESCRIPTION"
        descriptionTitleLabel.font = style.detail.descriptionTitleFont
        descriptionTitleLabel.textColor = style.detail.descriptionTitleColor
        descriptionTitleLabel.sizeToFit()
        contentView.addSubview(descriptionTitleLabel)
        
        let descriptionContentLabel = UILabel()
        descriptionContentLabel.frame = CGRect(x: 0, y: descriptionTitleLabel.frame.origin.y + descriptionTitleLabel.frame.height + 8, width: view.frame.width - 40, height: 19)
        descriptionContentLabel.text = movie.description
        descriptionContentLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        descriptionContentLabel.numberOfLines = 0
        descriptionContentLabel.sizeToFit()
        descriptionContentLabel.font = style.detail.descriptionContentFont
        descriptionContentLabel.textColor = style.detail.descriptionContentColor
        contentView.addSubview(descriptionContentLabel)
        
        let directorTitleLabel = UILabel()
        directorTitleLabel.frame = CGRect(x: 0, y: descriptionContentLabel.frame.origin.y + descriptionContentLabel.frame.height + 16, width: 200, height: 19)
        directorTitleLabel.text = "DIRECTOR"
        directorTitleLabel.font = style.detail.directorTitleFont
        directorTitleLabel.textColor = style.detail.directorTitleColor
        directorTitleLabel.sizeToFit()
        contentView.addSubview(directorTitleLabel)
        
        let directorContentLabel = UILabel()
        directorContentLabel.frame = CGRect(x: 0, y: directorTitleLabel.frame.origin.y + directorTitleLabel.frame.height + 8, width: view.frame.width - 40, height: 19)
        directorContentLabel.text = movie.director
        directorContentLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        directorContentLabel.numberOfLines = 0
        directorContentLabel.sizeToFit()
        directorContentLabel.font = style.detail.directorContentFont
        directorContentLabel.textColor = style.detail.directorContentColor
        contentView.addSubview(directorContentLabel)
        
        let starringTitleLabel = UILabel()
        starringTitleLabel.frame = CGRect(x: 0, y: directorContentLabel.frame.origin.y + directorContentLabel.frame.height + 16, width: 200, height: 19)
        starringTitleLabel.text = "STARRING"
        starringTitleLabel.font = style.detail.starringTitleFont
        starringTitleLabel.textColor = style.detail.starringTitleColor
        starringTitleLabel.sizeToFit()
        contentView.addSubview(starringTitleLabel)
        
        let starringContentLabel = UILabel()
        starringContentLabel.frame = CGRect(x: 0, y: starringTitleLabel.frame.origin.y + starringTitleLabel.frame.height + 8, width: view.frame.width - 40, height: 19)
        starringContentLabel.text = movie.starring
        starringContentLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        starringContentLabel.numberOfLines = 0
        starringContentLabel.sizeToFit()
        starringContentLabel.font = style.detail.starringContentFont
        starringContentLabel.textColor = style.detail.starringContentColor
        contentView.addSubview(starringContentLabel)
        
        contentView.frame = CGRect(x: 20, y: 453, width: view.frame.width, height: starringContentLabel.frame.origin.y + starringContentLabel.frame.height + 48)
        scrollView.addSubview(contentView)
        
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height: 480 + contentView.frame.height)

        buyButton.frame = CGRect(x: 0, y: view.frame.height - 48, width: view.frame.width, height: 48)
        buyButton.setTitle("BUY TICKET", for: .normal)
        buyButton.setTitleColor(.white, for: .normal)
        buyButton.backgroundColor = style.buyButton.backgroundColor
        buyButton.titleLabel?.font = style.buyButton.font
        buyButton.titleLabel?.textColor = style.buyButton.color
        buyButton.addTarget(self, action: #selector(self.buy), for: .touchUpInside)
        view.addSubview(buyButton)
    }

}
