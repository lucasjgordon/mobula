//
//  MovieViewController.swift
//  Mobula
//
//  Created by Lucas Gordon on 01/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

import UIKit
import SDWebImage

class MovieViewController: UIViewController {
    
    var collectionView: UICollectionView!
    var movies: [Movie] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    let movieService = MovieService()
    var searchBar: UISearchBar!
    var selectedCell: MovieCollectionViewCell?
    var style: Style!
    var transitionManager = TransitionManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        updateData()
    }

    func inject(style: Style) {
        self.style = style
    }
    
}

// MARK: Private methods
private extension MovieViewController {
    
    func setupView() {
        view.backgroundColor = style.backgroundColor
        UIApplication.shared.statusBarStyle = style.statusBarStyle
        
        let searchBarFrame = CGRect(x: 5, y: 30, width: self.view.frame.width - 10, height: 30)
        searchBar = UISearchBar(frame: searchBarFrame)
        searchBar.placeholder = "Search For Movies"
        searchBar.searchBarStyle = .minimal
        let searchBarTextField = searchBar.value(forKey: "searchField") as? UITextField
        searchBarTextField?.textColor = style.searchBar.color
        searchBarTextField?.font = style.searchBar.font
        view.addSubview(searchBar)
        
        let collectionViewLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewLayout.minimumLineSpacing = 0
        if style.tile.isPadded {
            collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
            collectionViewLayout.itemSize = CGSize(width: (self.view.frame.width - 40) / 2, height: 313)
        } else {
            collectionViewLayout.minimumInteritemSpacing = 1
            collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            collectionViewLayout.itemSize = CGSize(width: (self.view.frame.width - 1) / 2, height: 248)
        }
        
        var collectionViewFrame = CGRect(x: 0, y: 76, width: self.view.frame.width, height: self.view.frame.height - 76)
        if style.tile.isPadded {
            collectionViewFrame = CGRect(x: 10, y: 76, width: self.view.frame.width - 20, height: self.view.frame.height - 76)
        }
        collectionView = UICollectionView(frame: collectionViewFrame, collectionViewLayout: collectionViewLayout)
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsVerticalScrollIndicator = false
        collectionView.register(MovieCollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        view.addSubview(collectionView)
    }
    
    func updateData() {
        movieService.get { (movies, error) in
            if error != nil {
                let alertController = UIAlertController(title: "We Couldn't Find Any Movies", message: "Please try again later", preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(alertAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            self.movies = movies
        }
    }

}

// MARK: CollectionView Delegate
extension MovieViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? MovieCollectionViewCell else {
            return UICollectionViewCell()
        }

        let movie = self.movies[indexPath.row]

        cell.imageView.sd_setImage(with: movie.thumb_url)
        cell.imageView.layer.cornerRadius = style.tile.cornerRadius
        cell.imageView.backgroundColor = style.tile.backgroundColor
        cell.titleLabel.text = movie.title
        cell.titleLabel.font = style.tile.titleFont
        cell.titleLabel.textColor = style.tile.titleColor
        cell.subtitleLabel.text = "£16.00"
        cell.subtitleLabel.font = style.tile.subtitleFont
        cell.subtitleLabel.textColor = style.tile.subtitleColor
        cell.maskImageView.isHidden = !style.tile.hasOverlay
        cell.starsImageView.isHidden = !style.tile.hasRating

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCell = collectionView.cellForItem(at: indexPath) as? MovieCollectionViewCell
        let movieDetailVC = MovieDetailViewController()
        movieDetailVC.movie = movies[indexPath.row]
        movieDetailVC.inject(style: style)
        movieDetailVC.transitioningDelegate = self.transitionManager
        present(movieDetailVC, animated: true, completion: nil)
    }

}
