//
//  MovieCollectionViewCell.swift
//  Mobula
//
//  Created by Lucas Gordon on 01/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    var titleLabel: UILabel!
    var subtitleLabel: UILabel!
    var imageView: UIImageView!
    var maskImageView: UIImageView!
    var starsImageView: UIImageView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: 248))
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = true
        contentView.addSubview(imageView)
        
        maskImageView = UIImageView(frame: imageView.frame)
        maskImageView.image = UIImage(named: "Mask")
        maskImageView.contentMode = .scaleAspectFit
        contentView.addSubview(maskImageView)
        
        subtitleLabel = UILabel(frame: CGRect(x: 0, y: frame.size.height - 29, width: frame.size.width, height: 14))
        subtitleLabel.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        subtitleLabel.textAlignment = .left
        contentView.addSubview(subtitleLabel)
        
        titleLabel = UILabel(frame: CGRect(x: 0, y: subtitleLabel.frame.origin.y - 30, width: frame.size.width, height: 28))
        titleLabel.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        titleLabel.textAlignment = .left
        contentView.addSubview(titleLabel)
        
        starsImageView = UIImageView(frame: CGRect(x: 53, y: frame.size.height - 28, width: 65, height: 12))
        starsImageView.image = UIImage(named: "stars")
        starsImageView.contentMode = .scaleAspectFit
        contentView.addSubview(starsImageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
