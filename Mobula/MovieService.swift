//
//  MovieService.swift
//  Mobula
//
//  Created by Lucas Gordon on 01/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

import Foundation

struct MovieService {
    
    let networkService = NetworkService()

    func get(completion: @escaping (_ movies: [Movie], _ error: NetworkError?) -> Void) {
        networkService.get("https://static.grabble.com/misc/ios-test/movies.json") { (json, error) in
            if let error = error {
                completion([], error)
                return
            }
            
            guard
                let json = json as? [String: Any],
                let results = json["results"] as? [[String: Any]]
            else {
                completion([], .invalidResponse)
                return
            }
            
            var movies: [Movie] = []
            
            for item in results {
                guard
                    let id = item["id"] as? String,
                    let title = item["title"] as? String,
                    let description = item["description"] as? String,
                    let director = item["director"] as? String,
                    let starring = item["starring"] as? String,
                    let thumb_url_string = item["thumb_url"] as? String,
                    let thumb_url = URL(string: thumb_url_string),
                    let ticket_url_string = item["ticket_url"] as? String,
                    let ticket_url = URL(string: ticket_url_string),
                    let trailer_cover_url_string = item["trailer_cover_url"] as? String,
                    let trailer_cover_url = URL(string: trailer_cover_url_string),
                    let trailer_youtube_id = item["trailer_youtube_id"] as? String
                else {
                    continue
                }
                let movie = Movie(id: id, title: title, description: description, director: director, starring: starring, thumb_url: thumb_url, ticket_url: ticket_url, trailer_cover_url: trailer_cover_url, trailer_youtube_id: trailer_youtube_id)
                movies.append(movie)
            }

            DispatchQueue.main.async {
                completion(movies, nil)
            }
        }
    }

}
