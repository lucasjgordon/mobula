//
//  Style.swift
//  Mobula
//
//  Created by Lucas Gordon on 01/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

import UIKit

struct Style {
    
    let buyButton: BuyButton
    let detail: Detail
    let preview: Preview
    let searchBar: SearchBar
    let tile: Tile
    let backgroundColor: UIColor
    let statusBarStyle: UIStatusBarStyle
    
    struct BuyButton {
        let backgroundColor: UIColor
        let color: UIColor
        let font: UIFont
        let hasPadding: Bool
    }
    
    struct Detail {
        let titleFont: UIFont
        let titleColor: UIColor
        let runTimeFont: UIFont
        let runTimeColor: UIColor
        let genreFont: UIFont
        let genreColor: UIColor
        let priceFont: UIFont
        let priceColor: UIColor
        let imageBackgroundColor: UIColor
        let imageCornerRadius: CGFloat
        let descriptionTitleFont: UIFont
        let descriptionTitleColor: UIColor
        let descriptionContentFont: UIFont
        let descriptionContentColor: UIColor
        let directorTitleFont: UIFont
        let directorTitleColor: UIColor
        let directorContentFont: UIFont
        let directorContentColor: UIColor
        let starringTitleFont: UIFont
        let starringTitleColor: UIColor
        let starringContentFont: UIFont
        let starringContentColor: UIColor
    }
    
    struct Preview {
        let backgroundColor: UIColor
        let image: UIImage
    }
    
    struct SearchBar {
        let color: UIColor
        let font: UIFont
    }
    
    struct Tile {
        let backgroundColor: UIColor
        let cornerRadius: CGFloat
        let hasOverlay: Bool
        let hasRating: Bool
        let isPadded: Bool
        let subtitleFont: UIFont
        let subtitleColor: UIColor
        let titleFont: UIFont
        let titleColor: UIColor
    }
}
