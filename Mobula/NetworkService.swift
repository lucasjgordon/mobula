//
//  NetworkService.swift
//  Mobula
//
//  Created by Lucas Gordon on 01/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case emptyResponse
    case invalidResponse
    case networkError
}

struct NetworkService {

    func get(_ urlString: String, completion: @escaping (_ json: Any?, _ error: NetworkError?) -> Void) {
        guard let url = URL(string: urlString) else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard error == nil else {
                completion(nil, NetworkError.networkError)
                return
            }
            guard let data = data else {
                completion(nil, NetworkError.emptyResponse)
                return
            }
            
            let json = try! JSONSerialization.jsonObject(with: data, options: [])
            completion(json, nil)
        }

        task.resume()
    }

}
