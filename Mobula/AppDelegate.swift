//
//  AppDelegate.swift
//  Mobula
//
//  Created by Lucas Gordon on 01/04/2017.
//  Copyright © 2017 Lucas Gordon. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func colorWith(hex:String) -> UIColor {
        let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            return .clear
        }
        return UIColor(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        guard
            let path = Bundle.main.path(forResource: "Style", ofType: "plist"),
            let dict = NSDictionary(contentsOfFile: path) as? [String: Any]
        else {
            print("Error reading Style.plist")
            return true
        }
        
        guard
            let searchBarProperties = dict["searchBar"] as? [String: Any],
            let searchBarColor = searchBarProperties["color"] as? String,
            let searchBarFontFamily = searchBarProperties["fontFamily"] as? String,
            let searchBarFontSize = searchBarProperties["fontSize"] as? CGFloat,
            let searchBarFont = UIFont(name: searchBarFontFamily, size: searchBarFontSize)
        else {
            print("Error reading Search Bar properties")
            return true
        }
        
        let searchBar = Style.SearchBar(
            color: colorWith(hex: searchBarColor),
            font: searchBarFont
        )
        
        guard
            let tileProperties = dict["tile"] as? [String: Any],
            let tileBackgroundColor = tileProperties["backgroundColor"] as? String,
            let tileCornerRadius = tileProperties["cornerRadius"] as? CGFloat,
            let tileHasOverlay = tileProperties["hasOverlay"] as? Bool,
            let tileHasRating = tileProperties["hasRating"] as? Bool,
            let tileIsPadded = tileProperties["isPadded"] as? Bool,
            let tileSubtitleFontFamily = tileProperties["subtitleFontFamily"] as? String,
            let tileSubtitleFontSize = tileProperties["subtitleFontSize"] as? CGFloat,
            let tileSubtitleFont = UIFont(name: tileSubtitleFontFamily, size: tileSubtitleFontSize),
            let tileSubtitleColor = tileProperties["subtitleColor"] as? String,
            let tileTitleFontFamily = tileProperties["titleFontFamily"] as? String,
            let tileTitleFontSize = tileProperties["titleFontSize"] as? CGFloat,
            let tileTitleFont = UIFont(name: tileTitleFontFamily, size: tileTitleFontSize),
            let tileTitleColor = tileProperties["titleColor"] as? String
        else {
            print("Error reading Tile properties")
            return true
        }
        
        let tile = Style.Tile(
            backgroundColor: colorWith(hex: tileBackgroundColor),
            cornerRadius: tileCornerRadius,
            hasOverlay: tileHasOverlay,
            hasRating: tileHasRating,
            isPadded: tileIsPadded,
            subtitleFont: tileSubtitleFont,
            subtitleColor: colorWith(hex: tileSubtitleColor),
            titleFont: tileTitleFont,
            titleColor: colorWith(hex: tileTitleColor)
        )
        
        guard
            let previewProperties = dict["preview"] as? [String: Any],
            let previewBackgroundColor = previewProperties["backgroundColor"] as? String,
            let previewImageName = previewProperties["image"] as? String,
            let previewImage = UIImage(named: previewImageName)
        else {
            print("Error reading Preview properties")
            return true
        }
        
        let preview = Style.Preview(
            backgroundColor: colorWith(hex: previewBackgroundColor),
            image: previewImage
        )
        
        guard
            let detailProperties = dict["detail"] as? [String: Any],
            let detailTitleFontFamily = detailProperties["titleFontFamily"] as? String,
            let detailTitleFontSize = detailProperties["titleFontSize"] as? CGFloat,
            let detailTitleFont = UIFont(name: detailTitleFontFamily, size: detailTitleFontSize),
            let detailTitleColor = detailProperties["titleColor"] as? String,
            let detailRunTimeFontFamily = detailProperties["runTimeFontFamily"] as? String,
            let detailRunTimeFontSize = detailProperties["runTimeFontSize"] as? CGFloat,
            let detailRunTimeFont = UIFont(name: detailRunTimeFontFamily, size: detailRunTimeFontSize),
            let detailRunTimeColor = detailProperties["runTimeColor"] as? String,
            let detailGenreFontFamily = detailProperties["genreFontFamily"] as? String,
            let detailGenreFontSize = detailProperties["genreFontSize"] as? CGFloat,
            let detailGenreFont = UIFont(name: detailGenreFontFamily, size: detailGenreFontSize),
            let detailGenreColor = detailProperties["genreColor"] as? String,
            let detailPriceFontFamily = detailProperties["priceFontFamily"] as? String,
            let detailPriceFontSize = detailProperties["priceFontSize"] as? CGFloat,
            let detailPriceFont = UIFont(name: detailPriceFontFamily, size: detailPriceFontSize),
            let detailPriceColor = detailProperties["priceColor"] as? String,
            let detailImageBackgroundColor = detailProperties["imageBackgroundColor"] as? String,
            let detailImageCornerRadius = detailProperties["imageCornerRadius"] as? CGFloat,
            let detailDescriptionTitleFontFamily = detailProperties["descriptionTitleFontFamily"] as? String,
            let detailDescriptionTitleFontSize = detailProperties["descriptionTitleFontSize"] as? CGFloat,
            let detailDescriptionTitleFont = UIFont(name: detailDescriptionTitleFontFamily, size: detailDescriptionTitleFontSize),
            let detailDescriptionTitleColor = detailProperties["descriptionTitleColor"] as? String,
            let detailDescriptionContentFontFamily = detailProperties["descriptionContentFontFamily"] as? String,
            let detailDescriptionContentFontSize = detailProperties["descriptionContentFontSize"] as? CGFloat,
            let detailDescriptionContentFont = UIFont(name: detailDescriptionContentFontFamily, size: detailDescriptionContentFontSize),
            let detailDescriptionContentColor = detailProperties["descriptionContentColor"] as? String,
            let detailDirectorTitleFontFamily = detailProperties["directorTitleFontFamily"] as? String,
            let detailDirectorTitleFontSize = detailProperties["directorTitleFontSize"] as? CGFloat,
            let detailDirectorTitleFont = UIFont(name: detailDirectorTitleFontFamily, size: detailDirectorTitleFontSize),
            let detailDirectorTitleColor = detailProperties["directorTitleColor"] as? String,
            let detailDirectorContentFontFamily = detailProperties["directorContentFontFamily"] as? String,
            let detailDirectorContentFontSize = detailProperties["directorContentFontSize"] as? CGFloat,
            let detailDirectorContentFont = UIFont(name: detailDirectorContentFontFamily, size: detailDirectorContentFontSize),
            let detailDirectorContentColor = detailProperties["directorContentColor"] as? String,
            let detailStarringTitleFontFamily = detailProperties["starringTitleFontFamily"] as? String,
            let detailStarringTitleFontSize = detailProperties["starringTitleFontSize"] as? CGFloat,
            let detailStarringTitleFont = UIFont(name: detailStarringTitleFontFamily, size: detailStarringTitleFontSize),
            let detailStarringTitleColor = detailProperties["starringTitleColor"] as? String,
            let detailStarringContentFontFamily = detailProperties["starringContentFontFamily"] as? String,
            let detailStarringContentFontSize = detailProperties["starringContentFontSize"] as? CGFloat,
            let detailStarringContentFont = UIFont(name: detailStarringContentFontFamily, size: detailStarringContentFontSize),
            let detailStarringContentColor = detailProperties["starringContentColor"] as? String
        else {
            print("Error reading Detail properties")
            return true
        }
        
        let detail = Style.Detail(
            titleFont: detailTitleFont,
            titleColor: colorWith(hex: detailTitleColor),
            runTimeFont: detailRunTimeFont,
            runTimeColor: colorWith(hex: detailRunTimeColor),
            genreFont: detailGenreFont,
            genreColor: colorWith(hex: detailGenreColor),
            priceFont: detailPriceFont,
            priceColor: colorWith(hex: detailPriceColor),
            imageBackgroundColor: colorWith(hex: detailImageBackgroundColor),
            imageCornerRadius: detailImageCornerRadius,
            descriptionTitleFont: detailDescriptionTitleFont,
            descriptionTitleColor: colorWith(hex: detailDescriptionTitleColor),
            descriptionContentFont: detailDescriptionContentFont,
            descriptionContentColor: colorWith(hex: detailDescriptionContentColor),
            directorTitleFont: detailDirectorTitleFont,
            directorTitleColor: colorWith(hex: detailDirectorTitleColor),
            directorContentFont: detailDirectorContentFont,
            directorContentColor: colorWith(hex: detailDirectorContentColor),
            starringTitleFont: detailStarringTitleFont,
            starringTitleColor: colorWith(hex: detailStarringTitleColor),
            starringContentFont: detailStarringContentFont,
            starringContentColor: colorWith(hex: detailStarringContentColor)
        )
        
        guard
            let buyButtonProperties = dict["buyButton"] as? [String: Any],
            let buyButtonBackgroundColor = buyButtonProperties["backgroundColor"] as? String,
            let buyButtonFontFamily = buyButtonProperties["fontFamily"] as? String,
            let buyButtonFontSize = buyButtonProperties["fontSize"] as? CGFloat,
            let buyButtonFont = UIFont(name: buyButtonFontFamily, size: buyButtonFontSize),
            let buyButtonColor = buyButtonProperties["color"] as? String
        else {
            print("Error reading Buy Button properties")
            return true
        }
        
        let buyButton = Style.BuyButton(
            backgroundColor: colorWith(hex: buyButtonBackgroundColor),
            color: colorWith(hex: buyButtonColor),
            font: buyButtonFont,
            hasPadding: false
        )
        
        guard let backgroundColor = dict["backgroundColor"] as? String else {
            print("Error reading global properties")
            return true
        }
        
        let style = Style(
            buyButton: buyButton,
            detail: detail,
            preview: preview,
            searchBar: searchBar,
            tile: tile,
            backgroundColor: colorWith(hex:  backgroundColor),
            statusBarStyle: backgroundColor == "#FFFFFF" ? .default : .lightContent
        )
        
        let movieCollectionVC = MovieViewController()
        movieCollectionVC.inject(style: style)
        
        let navigationController = UINavigationController()
        navigationController.setViewControllers([movieCollectionVC], animated: true)
        navigationController.isNavigationBarHidden = true
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()

        return true
    }

}

